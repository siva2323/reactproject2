import React from "react"

export default function Navbar() {
    return (
        <div className="logoBox">
            <img src={require("./airbnb.png")} className="airBnbLogo" />
        </div>
    )
}