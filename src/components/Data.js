export default [
    {
        id: 1,
        title: "Life Lessons with Katie Zaferes",
        price: 136,
        coverImg: "guy.png",
        stats: {
            rating: 5.0,
            reviewCount: 6
        },
        location: "USA",
        openSpots: 0,
    },
    {
        id: 2,
        title: "Learn Wedding Photography",
        price: 125,
        coverImg: "wedding.png",
        stats: {
            rating: 5.0,
            reviewCount: 30
        },
        location: "USA",
        openSpots: 27,
    },
    {
        id: 3,
        title: "Group Mountain Biking",
        price: 50,
        coverImg: "mountainBike.png",
        stats: {
            rating: 4.8,
            reviewCount: 2
        },
        location: "USA",
        openSpots: 3,
    },
    {
        id: 1,
        title: "Life Lessons with Katie Zaferes",
        price: 136,
        coverImg: "guy.png",
        stats: {
            rating: 5.0,
            reviewCount: 6
        },
        location: "USA",
        openSpots: 0,
    },
    {
        id: 2,
        title: "Learn Wedding Photography",
        price: 125,
        coverImg: "wedding.png",
        stats: {
            rating: 5.0,
            reviewCount: 30
        },
        location: "USA",
        openSpots: 27,
    },
    {
        id: 3,
        title: "Group Mountain Biking",
        price: 50,
        coverImg: "mountainBike.png",
        stats: {
            rating: 4.8,
            reviewCount: 2
        },
        location: "USA",
        openSpots: 3,
    },
    {
        id: 1,
        title: "Life Lessons with Katie Zaferes",
        price: 136,
        coverImg: "guy.png",
        stats: {
            rating: 5.0,
            reviewCount: 6
        },
        location: "USA",
        openSpots: 0,
    },
    {
        id: 2,
        title: "Learn Wedding Photography",
        price: 125,
        coverImg: "wedding.png",
        stats: {
            rating: 5.0,
            reviewCount: 30
        },
        location: "USA",
        openSpots: 27,
    },
    {
        id: 3,
        title: "Group Mountain Biking",
        price: 50,
        coverImg: "mountainBike.png",
        stats: {
            rating: 4.8,
            reviewCount: 2
        },
        location: "USA",
        openSpots: 3,
    }
]