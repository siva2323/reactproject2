import React from "react"

export default function Card(props) {
    return (
        <div className="card">
            {props.openSpots === 0 && <div className="cardBadge">SOLD OUT</div>}
            {props.openSpots  > 5 && <div className="cardBadge">ONLINE</div>}
        <img src={props.img} className="card--image" />
        <div className="card--stats">
            <img src={require("./star.png")} className="card--star" />
            <span>{props.rating}</span>
            <span className="gray">({props.reviewCount}) • </span>
            <span className="gray">{props.location}</span>
        </div>
        <p className="imageDescription">{props.title}</p>
        <p><span className="bold">From ${props.price}</span> / person</p>
    </div>
    )
}