import React from "react"

export default function Main() {
    return (
        <nav className="mainBox">
            <img src={require("./nineImages.png")} className="nineImages" />
        <h1 className="mainTitle">Online Experiences</h1>
        <p className="titleDescription">Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.</p>
        </nav>
    )
}