import './App.css';
import Navbar from './components/Navbar';
import Main from './components/Main';
import Card from './components/Card';
import Data from "./components/Data";

function App() {
  const cards = Data.map(item => {
    return (
        <Card 
            img={item.coverImg}
            rating={item.stats.rating}
            reviewCount={item.stats.reviewCount}
            location={item.location}
            title={item.title}
            price={item.price}
            openSpots={item.openSpots}
        />
    )
})        
  return (
    <div className="container">
      <Navbar />
      <Main />
      <div className='everyElement'>
      {cards}
    </div>
    </div>
  );
}

export default App;
